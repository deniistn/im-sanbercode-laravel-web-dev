<?php

require_once('Animal.php');
require_once('Elephant.php');
require_once('Bull.php');

$animal = new Animal("Cow");
echo "Nama hewan : " . $animal->nama . "<br>";
echo "Mempunyai tangan : " . $animal->tangan . "<br>";
echo "Memimilki kaki : " . $animal->kaki . "<br>";
echo "Berada di habitat : " . $animal->habitat . "<br> <br>";


$elephant = new Elephant("Elephant");
echo "Nama hewan : " . $elephant->nama . "<br>"; 
echo "Mempunyai tangan " . $elephant->tangan . "<br>";
echo "Memiliki kaki : " . $elephant->kaki . "<br>";
echo "Berada di habitat : " . $elephant->habitat . "<br>";
echo $elephant->badan("besar"); "<br>";


$bull = new Bull("Bull");
echo "Nama hewan : " . $bull->nama . "<br>"; 
echo "Mempunyai tangan :  " . $bull->tangan . "<br>";
echo "Memiliki kaki : " . $bull->kaki . "<br>";
echo "Berada di habitat : " . $bull->habitat . "<br>";
echo $bull->badan("besar");


?>