@extends('layouts.master')
@section('title')
    Halaman Tampil Cast
@endsection
@section('sub-title')
    cast
@endsection
@section('content')

<a href="/cast/create" class="btn btn-primary btn-sm my-2">Tambah Cast</a>

<table class="table">
  <thead>
    <tr>
      <th scope="col">Nama</th>
      <th scope="col">Umur</th>
      <th scope="col">Bio</th>
    </tr>
  </thead>
  <tbody>
    @forelse ($cast as $item)
    <tr>
        
       <td>{{$item->nama}}</td>
       <td>{{$item->umur}}</td>

       <td>    
          <form action="/cast/{{$item->id}}" method="POST">
            @csrf
            @method('delete')
            <a href="/cast/{{$item->id}}" class="btn btn-info btn-sm">Detail</a>
            <a href="/cast/{{$item->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
            
            <input type="submit" value="Delete" class="btn btn-danger btn-sm"></input> 
          </form>
       </td>

    </tr>

    @empty

    <tr>
        
       <td>Data Cast</td>

    </tr>

    @endforelse
  </tbody>
</table>

@endsection